# Student Management System

This is a student management system built using Django 4 and Bootstrap 5.
Thanks to **Bob's Programming Academy** YouTube tutorial.

## Install required dependencies
From the terminal and within the project root directory, run the following command line.

```shell
pip install -r requirements.txt
```

## Links
- [Python](https://www.python.org/)
- [Django Project](https://www.djangoproject.com/)
- [Font Awesome](https://fontawesome.com/)
- [Font Awesome CDN](https://cdnjs.com/libraries/font-awesome)
- [Bootstrap](https://getbootstrap.com/)
- [Bootswatch](https://bootswatch.com/)
- [YouTube Tutorial](https://www.youtube.com/watch?v=EUMpUUXKvP0)